<?php
/**
 * @file
 * Methods to compress images with curl and handle the responses.
 */

/**
 * Curl request to tinypng.com to compress an image.
 *
 * @param string $key
 *   The TinyPNG.com API key.
 * @param string $input
 *   The absolute file path.
 * @param int $base_id
 *   The fid of the file from file_managed table.
 * @param string $temp_file
 *   The file name of the image used to temporarily store the image.
 * @param string $filemime
 *   The file mime type of the image.
 *
 * @return null|string
 *   Returns a message if an error occurred or null if successful.
 */
function tinypngjpg_send_request($key, $input, $base_id, $temp_file, $filemime) {
  $response = drupal_http_request('https://api.tinypng.com/shrink',
    array(
      'headers' => array(
        'Content-Type' => $filemime,
        'Authorization' => 'Basic ' . base64_encode($key),
      ),
      'method' => 'POST',
      'data' => file_get_contents($input),
      'ssl' => array(
        'verify_peer' => TRUE,
      ),
    )
  );

  if (isset($response->code) && $response->code == 201) {
    // Get the image location.
    if (isset($response->headers['location'])) {

      // Download the image.
      $is_saved = system_retrieve_file($response->headers['location'], $input, FALSE, FILE_EXISTS_REPLACE);

      // If saved then update the records.
      if ($is_saved !== FALSE) {
        tinypngjpg_add_record($response, $base_id);
        tinypngjpg_update_tinypng_allowance_info($response);
      }

      return NULL;
    }
  }
  else {
    return tinypngjpg_extract_error_message($response);
  }

  $message = array();
  $message[] = array('Error' => 'NoResponse');
  $message[] = array('Message' => t('Did not receive a response from server.'));

  return $message;
}


/**
 * Extracts the error message from the response from tinypng.com.
 *
 * @param string $response
 *   The response from tinypng.com.
 *
 * @return string
 *   Returns the error message from response.
 */
function tinypngjpg_extract_error_message($response) {
  if (isset($response->data)) {
    $json = json_decode($response->data);
  }

  $message = array();

  if (isset($json) && isset($json->error)) {
    $message['Error'] = $json->error;
  }
  else {
    $message['Error'] = 'Unknown';
  }
  if (isset($json) && isset($json->message)) {
    $message['Message'] = $json->message;
  }

  return $message;
}

/**
 * Update the file size of an image for the file_managed table.
 *
 * @param int $base_id
 *   The fid of the file from file_managed table.
 * @param int $output_size
 *   The file size of the compressed image.
 */
function tinypngjpg_update_file_manged_recored_file_size($base_id, $output_size) {
  db_update('file_managed')
    ->fields(array('filesize' => $output_size))
    ->condition('fid', $base_id, '=')
    ->execute();
}

/**
 * Pulls out the compression count for the response.
 *
 * @param object $response
 *   Response from tinypng.com.
 */
function tinypngjpg_update_tinypng_allowance_info($response) {
  if (isset($response->headers['compression-count'])) {
    variable_set('tinypngjpg_api_key_allowance', $response->headers['compression-count']);
  }
}

/**
 * Adds a record to the tinypngjpg_compressions table.
 *
 * @param object $response
 *   The json response from tinypng.com.
 * @param int $base_id
 *   The fid of the file from file_managed table.
 */
function tinypngjpg_add_record($response, $base_id) {
  if (isset($response->data)) {
    $json = json_decode($response->data, TRUE);
  }

  if (isset($json['input']['size'])) {
    $input_size = $json['input']['size'];
  }
  else {
    $input_size = -1;
  }

  if (isset($json['output']['size'])) {
    $output_size = $json['output']['size'];
    tinypngjpg_update_file_manged_recored_file_size($base_id, $output_size);
  }
  else {
    $output_size = -1;
  }

  if (isset($json['input']['size']) && isset($json['output']['size'])) {
    db_insert('tinypngjpg_compressions')
      ->fields(array(
        'base_id' => $base_id,
        'input_size' => $input_size,
        'output_size' => $output_size,
      ))
      ->execute();
  }
}

/**
 * Fetch sid from the base_id (fid).
 *
 * @param int $base_id
 *   The fid of the file from file_managed table.
 *
 * @return int
 *   Returns the sid of an atom from the fid supplied.
 */
function tinypngjpg_get_sid($base_id) {
  $result = db_select('scald_atoms', 's')
    ->fields('s')
    ->condition('base_id', $base_id, '=')
    ->execute()
    ->fetchAssoc();

  if ($result) {
    if (isset($result['sid'])) {
      return $result['sid'];
    }
  }

  return 0;
}

/**
 * Checks if an image has already been compressed.
 *
 * @param int $base_id
 *   The fid of the file from file_managed table.
 *
 * @return bool|string
 *   Returns the percentage compressed if already processed.
 */
function tinypngjpg_get_compressed_data($base_id) {
  $result = db_select('tinypngjpg_compressions', 'c')
    ->fields('c')
    ->condition('base_id', $base_id, '=')
    ->execute()
    ->fetchAssoc();

  if ($result) {
    if (isset($result['input_size']) && isset($result['output_size'])) {
      return round((1 - $result['output_size'] / $result['input_size']) * 100);
    }
    return TRUE;
  }

  return FALSE;
}

/**
 * Get the count of images not compressed according to compression table.
 *
 * @return mixed
 *   Returns the count of uncompressed images.
 */
function tinypngjpg_total_images_uncompressed() {
  $query = db_select('file_managed', 'f')
    ->fields('f', array('fid', 'filename'))
    ->condition('f.filemime', array('image/jpeg', 'image/png'), 'IN')
    ->condition('f.fid',
      db_select('tinypngjpg_compressions', 'c')->fields('c', array('base_id')),
      'NOT IN')
    ->execute();

  return $query->rowCount();
}

/**
 * Get fid (base_id) of all images that are uncompressed.
 *
 * @return array
 *   Returns array of fids for the uncompressed images.
 */
function tinypngjpg_all_uncompressed_images() {
  $images = array();

  $file_managed = db_select('file_managed', 'f')
    ->fields('f', array('fid', 'filename'))
    ->condition('f.filemime', array('image/jpeg', 'image/png'), 'IN')
    ->condition('f.fid',
      db_select('tinypngjpg_compressions', 'c')->fields('c', array('base_id')),
      'NOT IN')
    ->condition('f.fid',
      db_select('tinypngjpg_compression_failures', 'cf')
        ->fields('cf', array('base_id'))
        ->condition(
            'cf.error',
            array('BadSignature', 'UnsupportedFile', 'DecodeError'),
            'IN'),
      'NOT IN')
    ->execute();

  while ($img = $file_managed->fetchAssoc()) {
    $images[] = $img;
  }

  return $images;
}

/**
 * Save error to database so that we do not repeatedly process the image.
 *
 * @param int $fid
 *   The fid of the file from file_managed table.
 * @param array $error
 *   The error message.
 */
function tinypngjpg_save_error($fid, array $error) {
  db_insert('tinypngjpg_compression_failures')
    ->fields(array(
      'base_id' => isset($fid) ? $fid : 0,
      'error' => isset($error['Error']) ? $error['Error'] : 'NotSet',
      'message' => isset($error['Message']) ? $error['Message'] : 'Not Set',
      'created' => time(),
    ))
    ->execute();
}

/**
 * Check if fid is in the table.
 *
 * @param int $fid
 *   The fid of the file from file_managed table.
 *
 * @return bool
 *   Returns bool for checking in the failures table.
 */
function tinypngjpg_not_in_save_error($fid) {
  $query = db_select('tinypngjpg_compression_failures', 'cf')
    ->fields('cf', array('base_id'))
    ->condition('base_id', $fid, '=')
    ->execute()
    ->fetchAssoc();

  return empty($query) ? FALSE : TRUE;
}

/**
 * Clear the caches when using settings.
 */
function tinypngjpg_super_clear_cache() {
  cache_clear_all('*', 'cache_scald', TRUE);
  cache_clear_all('theme_registry:', 'cache', TRUE);
}

/**
 * Implements hook_init().
 *
 * @todo: Suggestions to move this in another location are welcome.
 */
function tinypngjpg_init() {
  if (variable_get('tinypngjpg_show_manual_compression', 1) == 1) {
    drupal_add_css(drupal_get_path('module', 'tinypngjpg') . '/assets/css/tinypngjpg-css.css');
  }
}
