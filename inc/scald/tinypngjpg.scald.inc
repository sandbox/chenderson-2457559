<?php
/**
 * @file
 * Methods related to scald.
 */

/**
 * Overriding function from scald module theme_sdl_library_item($variable).
 *
 * @param object $variables
 *   Variables based in by sdl.
 *
 * @return string
 *   Returns the rendered library item markup.
 */
function tinypngjpg_sdl_library_item($variables) {
  $atom = $variables['atom'];
  $image = $variables['image'];
  $informations = $atom->rendered;

  // Action links.
  $links = scald_atom_user_build_actions_links($atom, NULL);

  // Force all links to open in a new window.
  foreach ($links as $action => $link) {
    $links[$action]['attributes']['target'] = '_blank';
  }
  // The Insert link. Use the "_" prefix to avoid collision with possible
  // "insert" action.
  $links['_insert'] = array(
    'title' => t('Insert'),
    'external' => TRUE,
    'fragment' => FALSE,
    'attributes' => array(
      'data-atom-id' => $atom->sid,
      'style' => 'display:none',
    ),
    'href' => '',
  );
  $links_element = array(
    '#theme' => 'links',
    '#links' => $links,
    '#attributes' => array('class' => array('links', 'inline')),
  );
  $rendered_links = drupal_render($links_element);

  // Authors.
  if (!empty($informations->authors)) {
    foreach ($informations->authors as $author) {
      $author_names[] = check_plain($author->name);
    }
    $authors = implode(', ', $author_names);
  }
  else {
    $authors = '';
  }

  // Compression.
  $compression = '';
  if (isset($atom->file_source)) {
    $mime_type = file_get_mimetype($atom->file_source);

    if ($mime_type == 'image/jpeg' || $mime_type == 'image/png') {
      $compression = tinypngjpg_sdl_library_item_compression_info($atom);
    }
  }

  return "<div class='image'>{$image}</div>
  <div class='meta type-" . drupal_html_class($atom->type) . " clearfix'>
    <div class='title'>{$informations->title}</div>
    <div class='author'>{$authors}</div>
    {$rendered_links}
  </div>$compression
  ";
}

/**
 * Returns HTML ul for compression information of an atom.
 *
 * @param object $atom
 *   Atom for image.
 *
 * @return string
 *   Returns ul for compression information.
 */
function tinypngjpg_sdl_library_item_compression_info($atom) {
  if (isset($atom->base_id) && !tinypngjpg_not_in_save_error($atom->base_id)) {
    $compression_num = tinypngjpg_get_compressed_data($atom->base_id);
    if ($compression_num !== FALSE) {
      if ($compression_num === TRUE) {
        return '<ul class="links inline"><li>Compressed</li></ul>';
      }

      $img = theme('image', array(
        'path' => drupal_get_path('module', 'tinypngjpg') . '/assets/img/info.png',
        'alt' => 'tooltip information icon',
      ));

      $tooltip = 'Compressed to ' . format_size($atom->base_entity->filesize) . ', saving ' . $compression_num . '%';

      $i_tag = '<i data-tooltip="' . $tooltip . '">' . $img . '</i>';

      return '<ul class="links inline"><li>Compressed ' . $i_tag . '</li></ul>';
    }
    else {
      $form = drupal_get_form('tinypngjpg_compress_form', $atom->base_id, drupal_get_destination());
      return drupal_render($form);
    }
  }

  return '';
}

/**
 * Implements hook_theme_registry_alter().
 *
 * Used to override the function theme_sdl_library_item in scald module.
 *
 * @todo: Improve somehow.
 */
function tinypngjpg_theme_registry_alter(&$theme_registry) {
  if (!empty($theme_registry['sdl_library_item']) && variable_get('tinypngjpg_show_manual_compression', 1) == 1) {
    $theme_registry['sdl_library_item']['function'] = 'tinypngjpg_sdl_library_item';
  }
}

/**
 * Implements hook _form().
 */
function tinypngjpg_compress_form($form, &$form_state, $base_id = 0, $query = '') {
  $form['base_id'] = array(
    '#title' => 'Base ID',
    '#type' => 'hidden',
    '#element_validate' => array('element_validate_number'),
    '#value' => $base_id,
  );
  $form['query'] = array(
    '#type' => 'hidden',
    '#value' => $query,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Compress'),
    '#ajax' => array(
      'callback' => 'tinypngjpg_compress_form_callback',
      'wrapper' => 'tinypngjpg-ajax-response-' . $base_id,
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );
  $form['#attributes'] = array(
    'id' => 'tinypngjpg-ajax-response-' . $base_id,
  );
  $form['#action'] = url('/admin/config/tinypngjpg/compress', array('query' => $query));
  return $form;
}

/**
 * Implements hook _form_submit().
 */
function tinypngjpg_compress_form_submit($form, &$form_state) {
  tinypngjpg_compress($form_state['values']['base_id']);
}

/**
 * Callback for compression form, tinypngjpg_compress_form().
 */
function tinypngjpg_compress_form_callback($form, $form_state) {
  module_load_include('module', 'scald', 'scald');

  $atom = scald_atom_load(tinypngjpg_get_sid($form_state['values']['base_id']));
  $content = tinypngjpg_sdl_library_item_compression_info($atom);

  $commands = array();
  $commands[] = ajax_command_replace('#tinypngjpg-ajax-response-' . $form_state['values']['base_id'], $content);
  $commands[] = ajax_command_remove('div.messages.status');
  $commands[] = ajax_command_remove('div.messages.error');
  $commands[] = ajax_command_remove('div.messages.warning');
  $commands[] = ajax_command_prepend('.tabs-secondary', theme('status_messages'));

  return array('#type' => 'ajax', '#commands' => $commands);
}
