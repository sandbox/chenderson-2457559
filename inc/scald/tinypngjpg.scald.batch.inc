<?php
/**
 * @file
 * Compress images using batch method.
 */

/**
 * Compress an image for use with batch methods, returns error message or null.
 *
 * @param int $base_id
 *   The int id relating to fid from file_managed table.
 *
 * @return null|string
 *   Returns a message if an error occurred or null if successful.
 */
function tinypngjpg_compress_for_batch($base_id) {
  $key = variable_get('tinypngjpg_api_key');
  $file = file_load($base_id);

  if ($file && isset($file->uri)) {
    if ($wrapper = file_stream_wrapper_get_instance_by_uri($file->uri)) {
      $file_absolute_path = $wrapper->realpath();

      // Only send it jpeg or png.
      if ($file->filemime == 'image/jpeg' || $file->filemime == 'image/png') {
        return tinypngjpg_send_request($key, $file_absolute_path, $base_id, $file->filename, $file->filemime);
      }
    }
  }

  $message = array();
  $message[] = array('Error' => 'NotFound');
  $message[] = array('Message' => t('Could not find the file from the fid supplied fid: @base_id', array('@base_id' => $base_id)));

  return $message;
}

/**
 * Implements hook_form().
 *
 * Form for bulk import of images to compress.
 */
function tinypngjpg_bulk_import_form($form) {
  if (isset($_SESSION['my_batch_results'])) {
    $results = $_SESSION['my_batch_results'];

    $markup = '<ol>';
    foreach ($results as $result) {
      $markup .= '<li>' . check_plain($result) . '</li>';
    }
    $markup .= '</ol>';

    $form['description'] = array(
      '#markup' => $markup,
    );

    unset($_SESSION['my_batch_results']);
  }
  else {
    $total = tinypngjpg_total_images_uncompressed();

    $form['description'] = array(
      '#markup' => t('<p>Total number of images unprocessed: @total</p><p>Select amount to run in the batch and confirm.</p>',
        array('@total' => $total)),
    );
    $form['amount'] = array(
      '#type' => 'textfield',
      '#title' => t('Amount'),
      '#default_value' => 10,
      '#element_validate' => array('element_validate_integer_positive'),
      '#required' => TRUE,
    );
    $form['cancel'] = array(
      '#type' => 'submit',
      '#title' => t('Cancel'),
      '#value' => 'Cancel',
    );
    $form['confirm'] = array(
      '#type' => 'submit',
      '#title' => t('Confirm'),
      '#value' => 'Confirm',
    );
  }

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function tinypngjpg_bulk_import_form_submit($form, $form_state) {
  if (isset($form_state['clicked_button']['#value'])) {
    switch ($form_state['clicked_button']['#value']) {
      case 'Cancel':
        drupal_goto('admin/config/tinypngjpg');
        break;

      case 'Confirm':
        tinypngjpg_start_batch_process($form_state);
        break;

    }
  }
}

/**
 * Check if key exists and load in the images.
 */
function tinypngjpg_start_batch_process($amount, &$context) {
  $key = variable_get('tinypngjpg_api_key');
  if (!empty($key) && $key != 'API Key') {
    $images = tinypngjpg_all_uncompressed_images();

    if (isset($amount['values']['amount'])) {
      $images = array_slice($images, -$amount['values']['amount']);
    }
    else {
      $images = array_slice($images, -10);
    }

    tinypngjpg_batch($images);
  }
  else {
    watchdog('tinypngjpg', 'Batch process cannot run. No key supplied in settings', array(), WATCHDOG_INFO, NULL);
    drupal_set_message(t('Batch process cannot run. No key supplied in settings.'), 'error');
    $context['finished'] = 1;
  }
}

/**
 * Implements hook_batch().
 */
function tinypngjpg_batch($images) {
  $batch = array(
    'title' => t('Bulk compression of images'),
    'operations' => array(),
    'finished' => 'tinypngjpg_process_finished',
    'init_message' => t('Initializing...'),
    'progress_message' => t('Operation @current out of @total.'),
    'error_message' => t('Bulk compression has received an error.'),
  );

  foreach ($images as $image) {
    $batch['operations'][] = array(
      'tinypngjpg_process',
      array($image),
    );
  }

  batch_set($batch);
}

/**
 * Process for batch method tinypngjpg_batch.
 *
 * Callback for batch_process().
 *
 * @param array $image
 *   Part of the file_manged table giving the filename and fid.
 *   - fid: id from the file_manged table.
 *   - filename: file name.
 * @param string $context
 *   Context from batch processing.
 */
function tinypngjpg_process(array $image, &$context) {
  if (isset($image['fid'])) {
    $error = tinypngjpg_compress_for_batch($image['fid']);

    if (isset($error)) {
      watchdog('tinypngjpg', 'Compression failed: %error_type, %error_message', array('%error_type' => $error['Error'], '%error_message' => $error['Message']), WATCHDOG_ERROR, NULL);
      tinypngjpg_save_error($image['fid'], $error);
    }

    $file_name = isset($image['filename']) ? $image['filename'] : $image['fid'];

    if (isset($error)) {
      $context['results'][] = 'File: ' . $file_name . ', error. ' . $error['Error'] . ': ' . $error['Message'];
    }
    else {
      $context['results'][] = 'File: ' . $file_name . ', compressed.';
    }

    $context['finished'] = 1;
  }
}

/**
 * Process finished for batch method tinypngjpg_batch.
 */
function tinypngjpg_process_finished($success, $results, $operations) {
  if ($success) {
    $message = t('All images have been processed.');
    $_SESSION['my_batch_results'] = $results;
  }
  else {
    $message = t('Finished with error.');
  }

  tinypngjpg_super_clear_cache();
  drupal_set_message($message);
}
