<?php
/**
 * @file
 * Compress images using manual method.
 */

/**
 * Compress an image method to use with atom library view.
 *
 * @param int $base_id
 *   The int id relating to fid from file_managed table.
 * @param bool $automatic
 *   Optional boolean to decide if the request is dealt with automatic.
 */
function tinypngjpg_compress($base_id, $automatic = FALSE) {
  $key = variable_get('tinypngjpg_api_key');

  // Check if image has already been compressed.
  // @todo: Consider a switch statement here. Too much nesting.
  if (tinypngjpg_get_compressed_data($base_id) === FALSE) {
    // Check if the key is set.
    if (!empty($key) && $key != 'API Key') {
      $file = file_load($base_id);

      if ($file && isset($file->uri)) {
        if ($wrapper = file_stream_wrapper_get_instance_by_uri($file->uri)) {
          $file_absolute_path = $wrapper->realpath();

          // Only send it jpeg or png.
          if ($file->filemime == 'image/jpeg' || $file->filemime == 'image/png') {
            $error = tinypngjpg_send_request($key, $file_absolute_path, $base_id, $file->filename, $file->filemime);

            // Need to change the base_id (that is fid) into the sid.
            $sid = tinypngjpg_get_sid($base_id);
            cache_clear_all("{$sid}:sdl_library_item:", 'cache_scald', TRUE);

            if (isset($error)) {
              tinypngjpg_save_error($base_id, $error);
              watchdog('tinypngjpg', 'Compression failed: %error_type, %error_message', array('%error_type' => $error['Error'], '%error_message' => $error['Message']), WATCHDOG_ERROR, NULL);
              if (!$automatic) {
                drupal_set_message(check_plain('Compression failed: ' . $error['Error'] . ', ' . $error['Message']), 'error');
              }
            }
            elseif (!$automatic) {
              drupal_set_message(t('Compression successful.'), 'status');
            }
          }
        }
      }
      else {
        tinypngjpg_save_error($base_id, array('Error' => 'NotFound', 'Message' => 'Could not find the file from the fid supplied'));
        watchdog('tinypngjpg', 'Could not find the file from the fid supplied fid: %fid', array('%fid' => $base_id), WATCHDOG_ERROR, NULL);
        if (!$automatic) {
          drupal_set_message(t('Could not find the file from the fid supplied'), 'error');
        }
      }
    }
    else {
      watchdog('tinypngjpg', 'Compression process cannot run. No key supplied in settings', array(), WATCHDOG_INFO, NULL);
      if (!$automatic) {
        drupal_set_message(t('Compression process cannot run. No key supplied in settings.'), 'error');
      }
    }
  }
  else {
    watchdog('tinypngjpg', 'Compression process cannot run. Image already compressed.', array(), WATCHDOG_INFO, NULL);
    if (!$automatic) {
      drupal_set_message(t('Compression process cannot run. Image already compressed.'), 'error');
    }
  }
}
