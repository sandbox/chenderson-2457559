<?php
/**
 * @file
 * Compress images using automatic method.
 */

/**
 * Implements hook_scald_atom_insert().
 *
 * Auto compresses images uploaded via scald.
 */
function tinypngjpg_scald_atom_insert($atom) {
  if (variable_get('tinypngjpg_auto_compress', 1) == 1 && isset($atom->scald_thumbnail[LANGUAGE_NONE]['0']['fid'])) {
    tinypngjpg_compress($atom->scald_thumbnail[LANGUAGE_NONE]['0']['fid'], TRUE);
  }
}
