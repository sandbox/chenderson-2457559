Image Compression TinyPNG/JPG
-----------------------------

Contents of this file:

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Image Compression TinyPNG/JPG is designed for users to integrate easily with
an image compression tool that will make your image suitable for the web. It
uses TinyPNG.com’s API which means no complex installation on your server.

Currently it will integrate with Scald (https://www.drupal.org/project/scald)
and going forward with other media management tools.



REQUIREMENTS
------------
This module requires the following modules:
 * Scald version 7.x-1.3 or higher (if integrating with scald).

This module does require cURL to be installed on the server.


INSTALLATION
------------

Image Compression TinyPNG/JPG does not depend on any other module for setup
however it will require Scald to compress images within the atoms section.

 - Copy Image Compression TinyPNG/JPG into your module directory and then 
   enable on the admin modules page.

   
CONFIGURATION
-------------
 - Get an API Key from https://tinypng.com/developers and add it in the
   setting page /admin/config/tinypngjpg/settings
 - Tick “auto compress images” to have images compress on upload
 - Tick "show manual compression options” to have control within the atoms 
   view (/admin/content/atoms).

INSTRUCTIONS
------------
If you choose to auto compress then going forward you images will be compressed
if they are able to be processed.

You are able to have manual compression options enabled at the same time. This
feature is to allow you to compress historical images on your site and any
failed compressions that might be caused due to the API and not the image.

If you have a large quantity of images then you can batch process your images
using the bulk compression option in configuration
(/admin/config/tinypngjpg/bulk-compression). This may natural take time if you
choose a significant number.

If you require an allowance greater then 500 successful image compressions per
month then you are able to create a subscription directly with TinyPNG.com.

MAINTAINERS
-----------
Current maintainers:
 * Chris Henderson (chenderson) - https://www.drupal.org/user/3187961/
