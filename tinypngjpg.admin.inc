<?php
/**
 * @file
 * Administrative forms and functions for the Tinypngjpg module.
 */

/**
 * Page callback: Configures admin settings via system_settings_form().
 *
 * @see tinypngjpg_menu()
 */
function tinypngjpg_admin_settings($form) {
  $form['description'] = array(
    '#markup' => '<p>' . t('API key allowance used: @allowance', array('@allowance' => variable_get('tinypngjpg_api_key_allowance', '0'))) . '</p>',
  );
  $form['scald_options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Integration for Scald'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['scald_options']['tinypngjpg_auto_compress'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto compress images'),
    '#default_value' => variable_get('tinypngjpg_auto_compress', TRUE),
    '#description' => t('Enabling this option will mean all images that are uploaded will be processed and attempted to compress.'),
  );
  $form['scald_options']['tinypngjpg_show_manual_compression'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show manual compression options'),
    '#default_value' => variable_get('tinypngjpg_show_manual_compression', FALSE),
    '#description' => t('Enabling this option will allow you to compress images from the atom interface and view saving made on the images. This option will work but currently reloads the first page of the view so will be frustrating for page 10.'),
  );
  $form['tinypngjpg'] = array(
    '#type' => 'fieldset',
    '#title' => t('TinyPNG/JPG Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['tinypngjpg']['tinypngjpg_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Key'),
    '#default_value' => variable_get('tinypngjpg_api_key', 'API Key'),
    '#description' => t('This key is got from https://tinypng.com/developers and will allow 500 free compressions per month. You can get up payments to have a greater allowance.'),
  );

  // Included as cache on scald needs to be reset so the manual compression
  // options update.
  $form['#submit'][] = 'tinypngjpg_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Flushes the caches required to remove/show compression info.
 */
function tinypngjpg_admin_settings_submit() {
  tinypngjpg_super_clear_cache();
}
